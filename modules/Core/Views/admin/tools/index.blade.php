@extends ('admin.layouts.app')
@section ('content')
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    $hasAvailableTools = false;
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="d-flex justify-content-between mb20">
                    <h1 class="title-bar">{{__('Công cụ')}}</h1>
                </div>
                <div class="panel">
                    <div class="panel-body pd15">
                        <div class="row area-setting-row">
                            @if($user->hasPermissionTo('language_manage'))
                                @php $hasAvailableTools = true; @endphp
                                <div class="col-md-4">
                                    <div class="area-setting-item">
                                        <a class="setting-item-link" href="{{url('admin/module/language')}}">
                                            <span class="setting-item-media">
                                                <i class="icon ion-ios-globe"></i>
                                            </span>
                                            <span class="setting-item-info">
                                                <span class="setting-item-title">{{__("Ngôn ngữ")}}</span>
                                                <span class="setting-item-desc">{{__("Quản lý ngôn ngữ trang web của bạn")}}</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            @endif
                            @if($user->hasPermissionTo('language_translation'))
                                @php $hasAvailableTools = true; @endphp
                                <div class="col-md-4">
                                    <div class="area-setting-item">
                                        <a class="setting-item-link" href="{{url('admin/module/language/translations')}}">
                                            <span class="setting-item-media">
                                                <i class="icon ion-ios-globe"></i>
                                            </span>
                                            <span class="setting-item-info">
                                                <span class="setting-item-title">{{__("Bản dịch")}}</span>
                                                <span class="setting-item-desc">{{__("Quản lý bản dịch trang web của bạn")}}</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            @endif
                            @if($user->hasPermissionTo('system_log_view'))
                                @php $hasAvailableTools = true; @endphp
                                <div class="col-md-4">
                                    <div class="area-setting-item">
                                        <a class="setting-item-link" href="{{url('admin/logs')}}">
                                                <span class="setting-item-media">
                                                    <i class="icon ion-ios-nuclear"></i>
                                                </span>
                                            <span class="setting-item-info">
                                                <span class="setting-item-title">{{__("Trình xem nhật ký hệ thống")}}</span>
                                                <span class="setting-item-desc">{{__("Lượt xem và quản lý nhật ký hệ thống của trang web của bạn")}}</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            @endif
                            @if($user->hasPermissionTo('system_log_view'))
                                @php $hasAvailableTools = true; @endphp
                                <div class="col-md-4">
                                    <div class="area-setting-item">
                                        <a class="setting-item-link" href="{{route('core.admin.updater.index')}}">
                                        <span class="setting-item-media">
                                            <i class="icon ion-ios-nuclear"></i>
                                        </span>
                                            <span class="setting-item-info">
                                            <span class="setting-item-title">{{__("Người cập nhập")}}</span>
                                            <span class="setting-item-desc">{{__("Người cập nhập Booking Core")}}</span>
                                        </span>
                                        </a>
                                    </div>
                                </div>
                            @endif
                            @if($user->hasPermissionTo('plugin_manage'))
                                @php $hasAvailableTools = true; @endphp
                                <div class="col-md-4">
                                    <div class="area-setting-item">
                                        <a class="setting-item-link" href="{{route('core.admin.plugins.index')}}">
                                        <span class="setting-item-media">
                                            <i class="icon ion-md-color-wand"></i>
                                        </span>
                                            <span class="setting-item-info">
                                            <span class="setting-item-title">{{__("Plugins")}}</span>
                                            <span class="setting-item-desc">{{__("Plugins for Booking Core")}}</span>
                                        </span>
                                        </a>
                                    </div>
                                </div>
                            @endif
                            @if(!$hasAvailableTools)
                                <div class="col-md-12">
                                    {{__("Không có công cụ có sẵn")}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection