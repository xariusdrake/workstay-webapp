@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between mb20">
            <h1 class="title-bar">{{__("Tiện ích của chuyến tham quan")}}</h1>
        </div>
        @include('admin.message')
        <div class="row">
            <div class="col-md-4 mb40">
                <div class="panel">
                    <div class="panel-title">{{__("Thêm tiện ích")}}</div>
                    <div class="panel-body">
                        <form action="{{route('attraction.admin.attribute.store',['id'=>($row->id) ? $row->id : '-1','lang'=>request()->query('lang')])}}" method="post">
                            @csrf
                            @include('Attraction::admin/attribute/form',['parents'=>$rows])
                            <div class="">
                                <button class="btn btn-primary" type="submit">{{__("Thêm mới")}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="filter-div d-flex justify-content-between ">
                    <div class="col-left">
                        @if(!empty($rows))
                            <form method="post" action="{{url('admin/module/attraction/attribute/editAttrBulk')}}" class="filter-form filter-form-left d-flex justify-content-start">
                                {{csrf_field()}}
                                <select name="action" class="form-control">
                                    <option value="">{{__(" Tùy chọn ")}}</option>
                                    <option value="delete">{{__(" Xóa ")}}</option>
                                </select>
                                <button data-confirm="{{__("Bạn có chắc chắn?")}}" class="btn-info btn btn-icon dungdt-apply-form-btn" type="button">{{__('Chấp nhận')}}</button>
                            </form>
                        @endif
                    </div>
                    <div class="col-left">
                        <form method="get" action="{{url('/admin/module/attraction/attribute')}} " class="filter-form filter-form-right d-flex justify-content-end" role="search">
                            <input type="text" name="s" value="{{ Request()->s }}" class="form-control" placeholder="{{__("Tìm kiếm theo tên")}}">
                            <button class="btn-info btn btn-icon btn_search" id="search-submit" type="submit">{{__('Tìm kiếm')}}</button>
                        </form>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-title">{{__("Tất cả thuộc tính")}}</div>
                    <div class="panel-body">
                        <form class="bravo-form-item">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th width="60px"><input type="checkbox" class="check-all"></th>
                                    <th>{{__("Tên")}}</th>
                                    <th class="">{{__("Hành động")}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($rows) > 0)
                                    @foreach($rows as $row)
                                        <tr>
                                            <td><input type="checkbox" class="check-item" name="ids[]" value="{{$row->id}}"></td>
                                            <td class="title">
                                                <a href="{{url('admin/module/attraction/attribute/edit/'.$row->id)}}">{{$row->name}}</a>
                                            </td>
                                            <td>
                                                <a href="{{url('admin/module/attraction/attribute/edit/'.$row->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> {{__('Chỉnh sửa')}}
                                                </a>
                                                <a href="{{url('admin/module/attraction/attribute/terms/'.$row->id)}}" class="btn btn-sm btn-success"><i class="fa fa"></i> {{__("Quản lý Điều khoản")}}
                                                </a>

                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">{{__("Không có dữ liệu")}}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
