<div class="panel">
    <div class="panel-title"><strong>{{__("Nội dung của chuyến tham quan")}}</strong></div>
    <div class="panel-body">
        <div class="form-group">
            <label>{{__("Tiêu đề")}}</label>
            <input type="text" value="{{$translation->title}}" placeholder="{{__("Tiêu đề của chuyến tham quan")}}" name="title" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">{{__("Nội Dung")}}</label>
            <div class="">
                <textarea name="content" class="d-none has-ckeditor" cols="30" rows="10">{{$translation->content}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">{{__("Mô tả")}}</label>
            <div class="">
                <textarea name="short_desc" class="form-control" cols="30" rows="4">{{$translation->short_desc}}</textarea>
            </div>
        </div>
        @if(is_default_lang())
            <div class="form-group">
                <label class="control-label">{{__("Danh Mục")}}</label>
                <div class="">
                    <select name="category_id" class="form-control">
                        <option value="">{{__("-- Xin Hãy Chọn --")}}</option>
                        <?php
                        $traverse = function ($categories, $prefix = '') use (&$traverse, $row) {
                            foreach ($categories as $category) {
                                $selected = '';
                                if ($row->category_id == $category->id)
                                    $selected = 'selected';
                                printf("<option value='%s' %s>%s</option>", $category->id, $selected, $prefix . ' ' . $category->name);
                                $traverse($category->children, $prefix . '-');
                            }
                        };
                        $traverse($attraction_category);
                        ?>
                    </select>
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="control-label">{{__("Youtube Video")}}</label>
                <input type="text" name="video" class="form-control" value="{{$row->video}}" placeholder="{{__("Youtube link video")}}">
            </div> -->
            <div class="form-group">
                <label class="control-label">{{__("Thời gian")}}</label>
                <input type="text" name="duration" class="form-control" value="{{$row->duration}}" placeholder="{{__("Thời gian")}}">
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="control-label">{{__("Số lượng người tối thiểu trong chuyến tham quan")}}</label>
                        <input type="text" name="min_people" class="form-control" value="{{$row->min_people}}" placeholder="{{__("Số lượng người tối thiểu trong chuyến tham quan")}}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="control-label">{{__("Số lượng người tối đa trong chuyến tham quan")}}</label>
                        <input type="text" name="max_people" class="form-control" value="{{$row->max_people}}" placeholder="{{__("Số lượng người tối đa trong chuyến tham quan")}}">
                    </div>
                </div>
            </div>
        @endif
        <div class="form-group-item">
            <label class="control-label">{{__('Câu Hỏi Thường Gặp')}}</label>
            <div class="g-items-header">
                <div class="row">
                    <div class="col-md-5">{{__("Tiêu đề")}}</div>
                    <div class="col-md-5">{{__('Nội dung')}}</div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="g-items">
                @if(!empty($translation->faqs))
                    @php if(!is_array($translation->faqs)) $translation->faqs = json_decode($translation->faqs); @endphp
                    @foreach($translation->faqs as $key=>$faq)
                        <div class="item" data-number="{{$key}}">
                            <div class="row">
                                <div class="col-md-5">
                                    <input type="text" name="faqs[{{$key}}][title]" class="form-control" value="{{$faq['title']}}" placeholder="{{__('Ví dụ: Khi nào và ở đâu chuyến tham quan kết thúc?')}}">
                                </div>
                                <div class="col-md-6">
                                    <textarea name="faqs[{{$key}}][content]" class="form-control full-h" placeholder="...">{{$faq['content']}}</textarea>
                                </div>
                                <div class="col-md-1">
                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="text-right">
                <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> {{__('Thêm')}}</span>
            </div>
            <div class="g-more hide">
                <div class="item" data-number="__number__">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" __name__="faqs[__number__][title]" class="form-control" placeholder="{{__('Ví dụ: Khi nào và ở đâu chuyến tham quan kết thúc?')}}">
                        </div>
                        <div class="col-md-6">
                            <textarea __name__="faqs[__number__][content]" class="form-control full-h" placeholder="..."></textarea>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('Attraction::admin/attraction/include-exclude')
        @include('Attraction::admin/attraction/itinerary')
        @if(is_default_lang())
            <div class="form-group">
                <label class="control-label">{{__("Ảnh Banner")}}</label>
                <div class="form-group-image">
                    {!! \Modules\Media\Helpers\FileHelper::fieldUpload('banner_image_id',$row->banner_image_id) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{{__("Bộ Sưu Tập")}}</label>
                {!! \Modules\Media\Helpers\FileHelper::fieldGalleryUpload('gallery',$row->gallery) !!}
            </div>
        @endif
    </div>
</div>