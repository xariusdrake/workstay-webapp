<?php
namespace Modules\Attraction\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Modules\Attraction\Models\Attraction;
use Modules\Attraction\Models\AttractionCategory;
use Modules\Location\Models\Location;

class ListAttractions extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Tiêu đề')
                ],
                [
                    'id'        => 'desc',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Mô tả')
                ],
                [
                    'id'        => 'number',
                    'type'      => 'input',
                    'inputType' => 'number',
                    'label'     => __('Số lượng')
                ],
                [
                    'id'            => 'style',
                    'type'          => 'radios',
                    'label'         => __('Phong cách'),
                    'values'        => [
                        [
                            'value'   => 'normal',
                            'name' => __("Phổ thông")
                        ],
                        [
                            'value'   => 'carousel',
                            'name' => __("Slider Carousel")
                        ],
                        [
                            'value'   => 'box_shadow',
                            'name' => __("Box Shadow")
                        ]
                    ]
                ],
                [
                    'id'      => 'category_id',
                    'type'    => 'select2',
                    'label'   => __('Lọc theo Danh mục'),
                    'select2' => [
                        'ajax'  => [
                            'url'      => url('/admin/module/attraction/category/getForSelect2'),
                            'dataType' => 'json'
                        ],
                        'width' => '100%',
                        'allowClear' => 'true',
                        'placeholder' => __('-- Lựa chọn --')
                    ],
                    'pre_selected'=>url('/admin/module/attraction/category/getForSelect2?pre_selected=1')
                ],
                [
                    'id'      => 'location_id',
                    'type'    => 'select2',
                    'label'   => __('Lọc theo Địa điểm'),
                    'select2' => [
                        'ajax'  => [
                            'url'      => url('/admin/module/location/getForSelect2'),
                            'dataType' => 'json'
                        ],
                        'width' => '100%',
                        'allowClear' => 'true',
                        'placeholder' => __('-- Lựa chọn --')
                    ],
                    'pre_selected'=>url('/admin/module/location/getForSelect2?pre_selected=1')
                ],
                [
                    'id'            => 'order',
                    'type'          => 'radios',
                    'label'         => __('Order'),
                    'values'        => [
                        [
                            'value'   => 'id',
                            'name' => __("Ngày tạo")
                        ],
                        [
                            'value'   => 'title',
                            'name' => __("Tiêu đề")
                        ],
                    ]
                ],
                [
                    'id'            => 'order_by',
                    'type'          => 'radios',
                    'label'         => __('Xắp xếp'),
                    'values'        => [
                        [
                            'value'   => 'asc',
                            'name' => __("Giảm dần")
                        ],
                        [
                            'value'   => 'desc',
                            'name' => __("Tăng dần")
                        ],
                    ]
                ],
                [
                    'type'=> "checkbox",
                    'label'=>__("Chỉ các chuyến tham quan đặc trưng?"),
                    'id'=> "is_featured",
                    'default'=>true
                ]
            ]
        ]);
    }

    public function getName()
    {
        return __('Chuyến tham quan: Danh sách');
    }

    public function content($model = [])
    {
        $model_Attraction = Attraction::select("bravo_attractions.*")->with(['location','translations','hasWishList']);
        if(empty($model['order'])) $model['order'] = "id";
        if(empty($model['order_by'])) $model['order_by'] = "desc";
        if(empty($model['number'])) $model['number'] = 5;
        if (!empty($model['location_id'])) {
            $location = Location::where('id', $model['location_id'])->where("status","publish")->first();
            if(!empty($location)){
                $model_Attraction->join('bravo_locations', function ($join) use ($location) {
                    $join->on('bravo_locations.id', '=', 'bravo_attractions.location_id')
                        ->where('bravo_locations._lft', '>=', $location->_lft)
                        ->where('bravo_locations._rgt', '<=', $location->_rgt);
                });
            }
        }
        if (!empty($model['category_id'])) {
            $category_ids = [$model['category_id']];
            $list_cat = AttractionCategory::whereIn('id', $category_ids)->where("status","publish")->get();
            if(!empty($list_cat)){
                $where_left_right = [];
                foreach ($list_cat as $cat){
                    $where_left_right[] = " ( bravo_attraction_category._lft >= {$cat->_lft} AND bravo_attraction_category._rgt <= {$cat->_rgt} ) ";
                }
                $sql_where_join = " ( ".implode("OR" , $where_left_right)." )  ";
                $model_Attraction
                    ->join('bravo_attraction_category', function ($join) use($sql_where_join) {
                        $join->on('bravo_attraction_category.id', '=', 'bravo_attractions.category_id')
                            ->WhereRaw($sql_where_join);
                    });
            }
        }
        if(!empty($model['is_featured']))
        {
            $model_Attraction->where('is_featured',1);
        }
        $model_Attraction->orderBy("bravo_attractions.".$model['order'], $model['order_by']);
        $model_Attraction->where("bravo_attractions.status", "publish");
        $model_Attraction->with('location');
        $model_Attraction->groupBy("bravo_attractions.id");
        $list = $model_Attraction->limit($model['number'])->get();
        $data = [
            'rows'       => $list,
            'style_list' => $model['style'],
            'title'      => $model['title'] ?? "",
            'desc'      => $model['desc'] ?? "",
        ];
        return view('Attraction::frontend.blocks.list-attraction.index', $data);
    }
}
