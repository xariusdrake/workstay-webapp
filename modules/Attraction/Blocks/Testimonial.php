<?php
namespace Modules\Attraction\Blocks;

use Modules\Template\Blocks\BaseBlock;

class Testimonial extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Tiêu đề')
                ],
                [
                    'id'          => 'list_item',
                    'type'        => 'listItem',
                    'label'       => __('Danh sách'),
                    'title_field' => 'title',
                    'settings'    => [
                        [
                            'id'        => 'name',
                            'type'      => 'input',
                            'inputType' => 'text',
                            'label'     => __('Tên')
                        ],
                        [
                            'id'    => 'desc',
                            'type'  => 'textArea',
                            'label' => __('Mô tả')
                        ],
                        [
                            'id'        => 'number_star',
                            'type'      => 'input',
                            'inputType' => 'number',
                            'label'     => __('Số sao')
                        ],
                        [
                            'id'    => 'avatar',
                            'type'  => 'uploader',
                            'label' => __('Ảnh đại diện')
                        ],
                    ]
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('Danh sách chứng nhận');
    }

    public function content($model = [])
    {
        return view('Attraction::frontend.blocks.testimonial.index', $model);
    }
}