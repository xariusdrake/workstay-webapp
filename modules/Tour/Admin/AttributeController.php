<?php
namespace Modules\Tour\Admin;

use Illuminate\Http\Request;
use Modules\AdminController;
use Modules\Core\Models\Attributes;
use Modules\Core\Models\AttributesTranslation;
use Modules\Core\Models\Terms;
use Modules\Core\Models\TermsTranslation;

class AttributeController extends AdminController
{
    protected $attributesClass;
    protected $termsClass;
    public function __construct()
    {
        $this->setActiveMenu('admin/module/tour');
        parent::__construct();
        $this->attributesClass = Attributes::class;
        $this->termsClass = Terms::class;
    }

    public function index(Request $request)
    {
        $this->checkPermission('tour_manage_attributes');
        $listAttr = $this->attributesClass::where("service", 'tour');
        if (!empty($search = $request->query('s'))) {
            $listAttr->where('name', 'LIKE', '%' . $search . '%');
        }
        $listAttr->orderBy('created_at', 'desc');
        $data = [
            'rows'        => $listAttr->get(),
            'row'         => new $this->attributesClass(),
            'translation'    => new AttributesTranslation(),
            'breadcrumbs' => [
                [
                    'name' => __('Chuyến du lịch'),
                    'url'  => 'admin/module/tour'
                ],
                [
                    'name'  => __('Thuộc tính'),
                    'class' => 'active'
                ],
            ]
        ];
        return view('Tour::admin.attribute.index', $data);
    }

    public function edit(Request $request, $id)
    {
        $row = $this->attributesClass::find($id);
        if (empty($row)) {
            return redirect()->back()->with('error', __('Không tìm thấy thuộc tính!'));
        }
        $translation = $row->translateOrOrigin($request->query('lang'));
        $this->checkPermission('tour_manage_attributes');
        $data = [
            'translation'    => $translation,
            'enable_multi_lang'=>true,
            'rows'        => $this->attributesClass::where("service", 'tour')->get(),
            'row'         => $row,
            'breadcrumbs' => [
                [
                    'name' => __('Chuyến du lịch'),
                    'url'  => 'admin/module/tour'
                ],
                [
                    'name' => __('Thuộc tính'),
                    'url'  => 'admin/module/tour/attribute'
                ],
                [
                    'name'  => __('Thuộc tính: :name', ['name' => $row->name]),
                    'class' => 'active'
                ],
            ]
        ];
        return view('Tour::admin.attribute.detail', $data);
    }

    public function store(Request $request)
    {
        $this->checkPermission('tour_manage_attributes');
        $this->validate($request, [
            'name' => 'required'
        ]);
        $id = $request->input('id');
        if ($id) {
            $row = $this->attributesClass::find($id);
            if (empty($row)) {
                return redirect()->back()->with('error', __('Không tìm thấy thuộc tính!'));
            }
        } else {
            $row = new $this->attributesClass($request->input());
            $row->service = 'tour';
        }
        $row->fill($request->input());
        $res = $row->saveOriginOrTranslation($request->input('lang'));
        if ($res) {
            return redirect()->back()->with('success', __('Thuộc tính đã được lưu'));
        }
    }

    public function editAttrBulk(Request $request)
    {
        $this->checkPermission('tour_manage_attributes');
        $ids = $request->input('ids');
        $action = $request->input('action');
        if (empty($ids) or !is_array($ids)) {
            return redirect()->back()->with('error', __('Hãy chọn ít nhất 1 item [check_translate]!'));
        }
        if (empty($action)) {
            return redirect()->back()->with('error', __('Hãy chọn hành động!'));
        }
        if ($action == "delete") {
            foreach ($ids as $id) {
                $query = $this->attributesClass::where("id", $id);
                $query->first();
                if(!empty($query)){
                    $query->delete();
                }
            }
        }
        return redirect()->back()->with('success', __('Cập nhập thành công!'));
    }

    public function terms(Request $request, $attr_id)
    {
        $this->checkPermission('tour_manage_attributes');
        $row = $this->attributesClass::find($attr_id);
        if (empty($row)) {
            return redirect()->back()->with('error', __('Không tìm thấy điều khoản!'));
        }
        $listTerms = $this->termsClass::where("attr_id", $attr_id);
        if (!empty($search = $request->query('s'))) {
            $listTerms->where('name', 'LIKE', '%' . $search . '%');
        }
        $listTerms->orderBy('created_at', 'desc');
        $data = [
            'rows'        => $listTerms->paginate(20),
            'attr'        => $row,
            "row"         => new $this->termsClass(),
            'translation'    => new TermsTranslation(),
            'breadcrumbs' => [
                [
                    'name' => __('Chuyến du lịch'),
                    'url'  => 'admin/module/tour'
                ],
                [
                    'name' => __('Thuộc tính'),
                    'url'  => 'admin/module/tour/attribute'
                ],
                [
                    'name'  => __('Thuộc tính: :name', ['name' => $row->name]),
                    'class' => 'active'
                ],
            ]
        ];
        return view('Tour::admin.terms.index', $data);
    }

    public function term_edit(Request $request, $id)
    {
        $this->checkPermission('tour_manage_attributes');
        $row = $this->termsClass::find($id);
        if (empty($row)) {
            return redirect()->back()->with('error', __('Không tìm thấy điều khoản'));
        }
        $translation = $row->translateOrOrigin($request->query('lang'));
        $attr = $this->attributesClass::find($row->attr_id);
        $data = [
            'row'         => $row,
            'translation'    => $translation,
            'enable_multi_lang'=>true,
            'breadcrumbs' => [
                [
                    'name' => __('Chuyến du lịch'),
                    'url'  => 'admin/module/tour'
                ],
                [
                    'name' => __('Thuộc tính'),
                    'url'  => 'admin/module/tour/attribute'
                ],
                [
                    'name' => $attr->name,
                    'url'  => 'admin/module/tour/attribute/terms/' . $row->attr_id
                ],
                [
                    'name'  => __('Điều khoản: :name', ['name' => $row->name]),
                    'class' => 'active'
                ],
            ]
        ];
        return view('Tour::admin.terms.detail', $data);
    }

    public function term_store(Request $request)
    {
        $this->checkPermission('tour_manage_attributes');
        $this->validate($request, [
            'name' => 'required'
        ]);
        $id = $request->input('id');
        if ($id) {
            $row = $this->termsClass::find($id);
            if (empty($row)) {
                return redirect()->back()->with('error', __('Không tìm thấy điều khoản!'));
            }
        } else {
            $row = new $this->termsClass($request->input());
            $row->attr_id = $request->input('attr_id');
        }
        $row->fill($request->input());
        $res = $row->saveOriginOrTranslation($request->input('lang'));
        if ($res) {
            return redirect()->back()->with('success', __('Điều khoản đã được lưu'));
        }
    }

    public function editTermBulk(Request $request)
    {
        $this->checkPermission('tour_manage_attributes');
        $ids = $request->input('ids');
        $action = $request->input('action');
        if (empty($ids) or !is_array($ids)) {
            return redirect()->back()->with('error', __('Hãy chọn ít nhất 1 item [check_translate]!'));
        }
        if (empty($action)) {
            return redirect()->back()->with('error', __('Hãy chọn hành động!'));
        }
        if ($action == "delete") {
            foreach ($ids as $id) {
                $query = $this->termsClass::where("id", $id);
                $query->first();
                if(!empty($query)){
                    $query->delete();
                }
            }
        }
        return redirect()->back()->with('success', __('Cập nhập thành công!'));
    }



}
