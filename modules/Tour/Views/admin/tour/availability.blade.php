<div class="panel">
    <div class="panel-title"><strong>{{__("Có sẵn")}}</strong></div>
    <div class="panel-body">
        <h3 class="panel-body-title">{{__('Giờ mở cửa')}}</h3>
        <div class="form-group">
            <label>
                <input type="checkbox" name="enable_open_hours" @if(!empty($row->meta->enable_open_hours)) checked @endif value="1"> {{__('Cho phép giờ mở cửa')}}
            </label>
        </div>
        <?php $old = $row->meta->open_hours ?? [];?>
        <div class="table-responsive form-group" data-condition="enable_open_hours:is(1)">
            <table class="table">
                <thead>
                <tr>
                    <th>{{__('Cho phép?')}}</th>
                    <th>{{__('Ngày trong tuần')}}</th>
                    <th>{{__('Mở cửa')}}</th>
                    <th>{{__('Đóng cửa')}}</th>
                </tr>
                </thead>
                @for($i = 1 ; $i <=7 ; $i++)
                    <tr>
                        <td>
                            <input style="display: inline-block" type="checkbox" @if($old[$i]['enable']  ?? false ) checked @endif name="open_hours[{{$i}}][enable]" value="1">
                        </td>
                        <td><strong>
                                @switch($i)
                                    @case(1)
                                    {{__('Thứ 2')}}
                                    @break
                                    @case(2)
                                    {{__('Thứ 3')}}
                                    @break
                                    @case (3)
                                    {{__('Thứ 4')}}
                                    @break
                                    @case (4)
                                    {{__('Thứ 5')}}
                                    @break
                                    @case (5)
                                    {{__('Thứ 6')}}
                                    @break
                                    @case (6)
                                    {{__('Thứ 7')}}
                                    @break
                                    @case (7)
                                    {{__('Chủ Nhật')}}
                                    @break
                                @endswitch
                            </strong></td>
                        <td>
                            <select class="form-control" name="open_hours[{{$i}}][from]">
                                <?php
                                $time = strtotime('2019-01-01 00:00:00');
                                for($k = 0; $k <= 23; $k++):

                                $val = date('H:i', $time + 60 * 60 * $k);
                                ?>
                                <option @if(isset($old[$i]) and $old[$i]['from'] == $val) selected @endif value="{{$val}}">{{$val}}</option>

                                <?php endfor;?>
                            </select>
                        </td>
                        <td>
                            <select class="form-control" name="open_hours[{{$i}}][to]">
                                <?php
                                $time = strtotime('2019-01-01 00:00:00');
                                for($k = 0; $k <= 23; $k++):

                                $val = date('H:i', $time + 60 * 60 * $k);
                                ?>
                                <option @if(isset($old[$i]) and  $old[$i]['to'] == $val ) selected @endif value="{{$val}}">{{$val}}</option>

                                <?php endfor;?>
                            </select>
                        </td>
                    </tr>
                @endfor
            </table>
        </div>
    </div>
</div>