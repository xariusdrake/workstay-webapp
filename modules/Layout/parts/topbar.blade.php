<div class="bravo_topbar">
    <div class="container">
        <div class="content">
        <div class="topbar-left">

              {!! setting_item_with_lang("topbar_left_text") !!}  


            </div> 
            <div class="topbar-right">
                <ul class="topbar-items">
                    @include('Core::frontend.currency-switcher')
                    @include('Language::frontend.switcher')
                @if(!Auth::id())
                        <li class="login-item">
                            <a href="#login" data-toggle="modal" data-target="#login" class="login">{{__('Đăng nhập')}}</a>
                        </li>
                    @else
                        <li class="login-item dropdown">
                            <a href="#" data-toggle="dropdown" class="login">{{__("Xin chào, :Name",['name'=>Auth::user()->getDisplayName()])}}
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu text-left">
                             {{--    @if(Auth::user()->hasPermissionTo('dashboard_vendor_access'))
                                <li><a href="{{url(app_get_locale().'/user/dashboard')}}"><i class="icon ion-md-analytics"></i> {{__("Tổng quan nhà cung cấp")}}</a></li>
                                @endif --}}
                                <li class="@if(Auth::user()->hasPermissionTo('dashboard_vendor_access')) menu-hr @endif">
                                    <a href="{{url(app_get_locale().'/user/profile')}}"><i class="icon ion-md-construct"></i> {{__("Thông tin cá nhân")}}</a>
                                </li> 
                                <li class="menu-hr"><a href="{{url(app_get_locale().'/user/booking-history')}}"><i class="fa fa-clock-o"></i> {{__("Lịch sử đặt phòng")}}</a></li>
                                <li class="menu-hr"><a href="{{url(app_get_locale().'/user/profile/change-password')}}"><i class="fa fa-lock"></i> {{__("Thay đổi mật khẩu")}}</a></li>
                                @if(Auth::user()->hasPermissionTo('dashboard_access'))
                                    <li class="menu-hr"><a href="{{url('/admin')}}"><i class="icon ion-ios-ribbon"></i> {{__("Tổng quan quản trị viên")}}</a></li>
                                @endif
                                <li class="menu-hr">
                                    <a  href="#" onclick="event.preventDefault(); document.getElementById('logout-form-topbar').submit();"><i class="fa fa-sign-out"></i> {{__('Đăng xuất')}}</a>
                                </li>
                            </ul>
                            <form id="logout-form-topbar" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
