<?php
namespace Modules\Vendor\Blocks;

use Modules\Template\Blocks\BaseBlock;

class VendorRegisterForm extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Tiêu đề')
                ],
                [
                    'id'        => 'desc',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Mô tả')
                ],
                [
                    'id'        => 'youtube',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Youtube link')
                ],
                [
                    'id'    => 'bg_image',
                    'type'  => 'uploader',
                    'label' => __('Trình đăng ảnh nền')
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('Mẫu đăng ký nhà cung cấp');
    }

    public function content($model = [])
    {
        return view('Vendor::frontend.blocks.form-register.index', $model);
    }
}
