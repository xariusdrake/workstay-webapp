@extends('Email::layout')
@section('content')
    <div class="b-container">
        <div class="b-panel">
            @if($to == 'vendor')
                <h1>{{__("Xin chào :name",['name'=>$user->getDisplayName()])}}</h1>

                @if($action == 'insert')
                    <p>{{__('Yêu cầu thanh toán của bạn đã được gửi:')}}</p>
                @elseif($action ==  'update')
                    <p>{{__('Yêu cầu thanh toán của bạn đã được cập nhập:')}}</p>
                @elseif($action ==  'reject')
                    <p>{{__('Yêu cầu thanh toán của bạn đã bị từ chối:')}}</p>
                @elseif($action ==  'delete')
                    <p>{{__('Yêu cầu thanh toán của bạn đã bị xóa')}}</p>
                @endif

                @if(!in_array($action,['insert','delete']))
                    <ul>
                        <li><strong>{{__('Trạng thái:')}}</strong> <strong>{{$payout_request->status_text}}</strong></li>
                        @if($payout_request->pay_date)
                            <li><strong>{{__('Ngày thanh toán:')}}</strong> <strong>{{display_date($payout_request->pay_date)}}</strong></li>
                        @endif
                        @if($payout_request->note_to_vendor)
                            <li><strong>{{__('Lời nhắn đến chủ nhà:')}}</strong> <strong>{!! clean($payout_request->note_to_vendor) !!}</strong></li>
                        @endif
                    </ul>
                    <p>{{__('Thông tin thanh toán:')}}</p>
                @endif

                <ul>
                    <li><strong>{{__("Payout ID [check_translate]:")}}</strong> <strong>#{{$payout_request->id}}</strong></li>
                    <li><strong>{{__('Số tiền: ')}}</strong> <strong>{{format_money($payout_request->amount)}}</strong></li>
                    <li><strong>{{__('Phương thức thanh toán: ')}}</strong>
                        {{__(':name đến :info',['name'=>$payout_request->payout_method_name,'info'=>$payout_request->account_info])}}
                    </li>
                    @if($payout_request->note_to_admin)
                    <li><strong>{{__('Lời nhắn đến quản lý: ')}}</strong>
                        {{$payout_request->note_to_admin}}
                    </li>
                    @endif
                    <li><strong>{{__('Được tạo ngày: ')}}</strong>
                        {{display_datetime($payout_request->created_at)}}
                    </li>

                </ul>
                <p>{{__('Bạn có thể kiểm tra yêu cầu thanh toán của mình tại đây:')}} <a class="btn btn-primary" target="_blank" href="{{route('vendor.payout.index')}}">{{__('Xem thanh toán')}}</a></p>

                <br>
            @else
                <h1>{{__("Xin chào quản trị viên")}}</h1>

                @if($action == 'insert')
                    <p>{{__('Chủ nhà đã gửi yêu cầu thanh toán:')}}</p>
                @elseif($action ==  'update')
                    <p>{{__('Yêu cầu thanh toán đã được cập nhập:')}}</p>
                @elseif($action ==  'reject')
                    <p>{{__('Yêu cầu thanh toán đã bị từ chối:')}}</p>
                @elseif($action ==  'delete')
                    <p>{{__('Yêu cầu thanh toán đã bị xóa')}}</p>
                @endif

                @if(!in_array($action,['insert','delete']))
                    <ul>
                        <li><strong>{{__('Trạng thái:')}}</strong> <strong>{{$payout_request->status_text}}</strong></li>
                        @if($payout_request->pay_date)
                            <li><strong>{{__('Ngày thanh toán:')}}</strong> <strong>{{display_date($payout_request->pay_date)}}</strong></li>
                        @endif
                        @if($payout_request->note_to_vendor)
                        <li><strong>{{__('Lời nhắn đến chủ nhà:')}}</strong> <strong>{!! clean($payout_request->note_to_vendor) !!}</strong></li>
                        @endif
                    </ul>
                    <p>{{__('Thông tin thanh toán:')}}</p>
                @endif

                <ul>
                    <li><strong>{{__("Payout ID:")}}</strong> <strong>#{{$payout_request->id}}</strong></li>
                    <li><strong>{{__('Chủ nhà: ')}}</strong> <strong><a target="_blank" href="{{route('user.profile',['id'=>$payout_request->vendor_id])}}">{{$payout_request->vendor->getDisplayName()}}</a></strong></li>
                    <li><strong>{{__('Số tiền: ')}}</strong> <strong>{{format_money($payout_request->amount)}}</strong></li>
                    <li><strong>{{__('Phương thức thanh toán: ')}}</strong>
                        {{__(':name to :info',['name'=>$payout_request->payout_method_name,'info'=>$payout_request->account_info])}}
                    </li>
                    @if($payout_request->note_to_admin)
                    <li><strong>{{__('Lời nhắn:')}}</strong>
                        {{$payout_request->note_to_admin}}
                    </li>
                    @endif
                    <li><strong>{{__('Ngày tạo:')}}</strong>
                        {{display_datetime($payout_request->created_at)}}
                    </li>

                </ul>
                <p>{{__('Bạn có thể kiểm tra toàn bộ yêu cầu thanh toán tại đây:')}} <a class="btn btn-primary" target="_blank" href="{{route('vendor.admin.payout.index')}}">{{__('Quản lý thanh toán')}}</a></p>
                <br>
            @endif
            <p>{{__('Trân trọng')}},<br>{{setting_item('site_title')}}</p>
        </div>
    </div>
@endsection
