<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__('Thiết lập chủ nhà')}}</h3>
        <p class="form-group-desc">{{__('Thay đổi hệ thống thiết lập chủ nhà')}}</p>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <div class="form-controls">
                            <div class="form-group">
                                <label> <input type="checkbox" @if($settings['vendor_enable'] ?? '' == 1) checked @endif name="vendor_enable" value="1"> {{__("Kích hoạt?")}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" data-condition="vendor_enable:is(1)">
                        <label>{{__('Dạng hoa hồng')}}</label>
                        <div class="form-controls">
                            <select name="vendor_commission_type" class="form-control">
                                <option value="percent" {{($settings['vendor_commission_type'] ?? '') == 'percent' ? 'selected' : ''  }}>{{__('Phần trăm')}}</option>
                                <option value="amount" {{($settings['vendor_commission_type'] ?? '') == 'amount' ? 'selected' : ''  }}>{{__('Số tiền')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" data-condition="vendor_enable:is(1)">
                        <label>{{__('Giá trị hoa hồng')}}</label>
                        <div class="form-controls">
                            <input type="text" class="form-control" name="vendor_commission_amount" value="{{!empty($settings['vendor_commission_amount'])?$settings['vendor_commission_amount']:"0" }}">
                        </div>
                        <p><i>{{__('Ví dụ: 10% hoa hồng. Chủ nhà nhận 90%, Quản lý nhận 10%')}}</i></p>
                    </div>
                @else
                    <p>{{__('Bạn có thể chỉnh sửa trên ngôn ngữ chính.')}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__('Đăng ký Chủ nhà')}}</h3>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <div class="form-controls">
                            <div class="form-group">
                                <label> <input type="checkbox" @if($settings['vendor_auto_approved'] ?? '' == 1) checked @endif name="vendor_auto_approved" value="1"> {{__("Chủ nhà tự động phê duyệt?")}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{__('Vai trò chủ nhà')}}</label>
                        <div class="form-controls">
                            <select name="vendor_role" class="form-control">

                                @foreach(\Spatie\Permission\Models\Role::all() as $role)
                                <option value="{{$role->id}}" {{($settings['vendor_role'] ?? '') == $role->id ? 'selected': ''  }}>{{ucfirst($role->name)}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                @else
                    <p>{{__('Bạn có thể chỉnh sửa trên ngôn ngữ chính.')}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__('Hồ sơ chủ nhà')}}</h3>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <div class="form-controls">
                            <div class="form-group">
                                <label> <input type="checkbox" @if($settings['vendor_show_email'] ?? '' == 1) checked @endif name="vendor_show_email" value="1"> {{__("Hiển thị email chủ nhà trong hồ sơ?")}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-controls">
                            <div class="form-group">
                                <label> <input type="checkbox" @if($settings['vendor_show_phone'] ?? '' == 1) checked @endif name="vendor_show_phone" value="1"> {{__("Hiển thị số điện thoại chủ nhà trong hồ sơ?")}}</label>
                            </div>
                        </div>
                    </div>
                @else
                    <p>{{__('Bạn có thể chỉnh sửa trên ngôn ngữ chính.')}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
@if(is_default_lang())
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__('Tùy chọn thanh toán')}}</h3>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-title"><strong>{{__("Tùy chọn thanh toán")}}</strong></div>
            <div class="panel-body">

                <div class="form-group">
                    <div class="form-controls">
                        <label ><strong>{{__("Vô hiệu hóa Module thanh toán?")}}</strong></label>
                        <div class="form-group">
                            <label> <input type="checkbox" @if(setting_item('disable_payout') == 1) checked @endif name="disable_payout" value="1"> {{__("Đồng ý")}}</label>
                        </div>
                    </div>
                </div>

                @php $vendor_payout_booking_status = setting_item('vendor_payout_booking_status','',true) @endphp
                <div class="form-group">
                    <label><strong>{{__("Tình trạng đặt phòng")}}</strong></label>
                    <div class="form-controls">
                        <div><label><input name="vendor_payout_booking_status[]" @if(in_array('processing',$vendor_payout_booking_status)) checked @endif type="checkbox" value="processing"> {{__("Đang chờ xử lý")}}</label></div>
                        <div><label><input  name="vendor_payout_booking_status[]" @if(in_array('confirmed',$vendor_payout_booking_status)) checked @endif type="checkbox" value="confirmed"> {{__("Đã xác nhận")}}</label></div>
                        <div><label><input  name="vendor_payout_booking_status[]" @if(in_array('completed',$vendor_payout_booking_status)) checked @endif type="checkbox" value="completed"> {{__("Đã hoàn thành")}}</label></div>
                        <div><label><input  name="vendor_payout_booking_status[]" @if(in_array('paid',$vendor_payout_booking_status)) checked @endif type="checkbox" value="paid"> {{__("Đã trả")}}</label></div>
                    </div>
                    <p><i>{{__("Lựa chọn trạng thái đặt phòng sẽ được sử dụng để tính toán thanh toán của chủ nhà")}}</i></p>
                </div>
                <div class="form-group">
                    <label><strong>{{__("Phương thức thanh toán")}}</strong></label>
                    <div class="form-controls">
                        <div class="form-group-item">
                            <div class="form-group-item">
                                <div class="g-items-header">
                                    <div class="row">
                                        <div class="col-md-1">{{__('ID')}}</div>
                                        <div class="col-md-8">{{__("Tên")}}</div>
                                        <div class="col-md-2">{{__('Yêu cầu')}}</div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="g-items">
                                    <?php
                                    $items = json_decode(setting_item('vendor_payout_methods'));
                                    if(empty($items) or !is_array($items))
                                        $items = [];
                                    ?>
                                    @foreach($items as $key=>$item)
                                        <div class="item" data-number="{{$key}}">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input placeholder="{{__('Eg: bank_transfer')}}" type="text" name="vendor_payout_methods[{{$key}}][id]" class="form-control" value="{{$item->id}}" >
                                                </div>
                                                <div class="col-md-6">
                                                    <label >{{__("Tên")}}</label>
                                                    <input type="text" name="vendor_payout_methods[{{$key}}][name]" class="form-control" value="{{$item->name}}">
                                                    <label >{{__("Mô tả")}}</label>
                                                    <textarea  name="vendor_payout_methods[{{$key}}][desc]" class="form-control" cols="30" rows="4">{{$item->desc}}</textarea>
                                                    <label >{{__("Tối thiểu để trả tiền")}}</label>
                                                    <input type="text" name="vendor_payout_methods[{{$key}}][min]" class="form-control" value="{{$item->min ?? ''}}">
                                                </div>

                                                <div class="col-md-2">
                                                    <input type="number" name="vendor_payout_methods[{{$key}}][order]" class="form-control" value="{{$item->order}}" >
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="text-right">
                                    <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> {{__('Thêm [check_translate]')}}</span>
                                </div>
                                <div class="g-more hide">
                                    <div class="item" data-number="__number__">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input placeholder="{{__('Eg: bank_transfer')}}" type="text" __name__="vendor_payout_methods[__number__][id]" class="form-control" value="" >
                                            </div>
                                            <div class="col-md-6">
                                                <label >{{__("Tên")}}</label>
                                                <input type="text" __name__="vendor_payout_methods[__number__][name]" class="form-control" value="">
                                                <label >{{__("Mô tả")}}</label>
                                                <textarea  __name__="vendor_payout_methods[__number__][desc]" class="form-control" cols="30" rows="4"></textarea>
                                                <label >{{__("Tối thiểu để trả tiền")}}</label>
                                                <input type="text" __name__="vendor_payout_methods[__number__][min]" class="form-control" value="">
                                            </div>

                                            <div class="col-md-2">
                                                <input type="number" __name__="vendor_payout_methods[__number__][order]" class="form-control" value="" >
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
