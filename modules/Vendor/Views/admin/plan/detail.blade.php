@extends('admin.layouts.app')

@section('content')
    <form action="" method="post">
        @csrf
        <div class="container-fluid">
            <div class="d-flex justify-content-between mb20">
                <div class="">
                    <h1 class="title-bar">{{$row->id ? __('Chỉnh sửa: ') .$row->title :  __('Thêm kế hoạch mới') }}</h1>
                    @if($row->slug)
                        <p class="item-url-demo">{{ __('Permalink: ')}} {{ url( config('page.page_route_prefix') )}}/<a href="#" class="open-edit-input" data-name="slug">{{$row->slug}}</a>
                        </p>
                    @endif
                </div>
                <div class="">
                    @if($row->slug)
                        <a class="btn btn-primary btn-sm" href="{{$row->getDetailUrl()}}" target="_blank">{{ __('Xem trang')}}</a>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="panel-body-title">{{ __('Nội dung kế hoạch')}}</h3>
                            <div class="form-group">
                                <label>{{ __('Tên')}}</label>
                                <input type="text" value="{{$row->name}}" placeholder="Tên" name="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>{{ __('Hoa hồng (%)')}}</label>
                                <input type="number" value="{{$row->base_commission}}" min="1" max="100" placeholder="Hoa hồng" name="base_commission" class="form-control">
                            </div>
                        </div>
                    </div>
                    @include("Vendor::admin.plan.services")
                </div>
                <div class="col-md-3">
                    <div class="panel">
                        <div class="panel-title"><strong>{{__('Công khai')}}</strong></div>
                        <div class="panel-body">
                            <div>
                                <label><input @if($row->status=='publish') checked @endif type="radio" name="status" value="publish"> {{__("Công khai")}}
                                </label></div>
                            <div>
                                <label><input @if($row->status=='draft') checked @endif type="radio" name="status" value="draft"> {{__("Bản nháp")}}
                                </label></div>
                            <div class="text-right">
                                <button class="btn btn-primary" type="submit">{{__('Lưu')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection


@section ('script.body')
@endsection
