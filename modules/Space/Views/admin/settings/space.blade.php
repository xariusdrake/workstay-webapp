<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__("Trang tìm kiếm")}}</h3>
        <p class="form-group-desc">{{__('Cấu hình trang tìm kiếm cho trang web của bạn')}}</p>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-title"><strong>{{__("Tùy chọn cung")}}</strong></div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="" >{{__("Tiêu đề trang")}}</label>
                    <div class="form-controls">
                        <input type="text" name="space_page_search_title" value="{{setting_item_with_lang('space_page_search_title',request()->query('lang'))}}" class="form-control">
                    </div>
                </div>
                @if(is_default_lang())
                <div class="form-group">
                    <label class="" >{{__("Trang Banner")}}</label>
                    <div class="form-controls form-group-image">
                        {!! \Modules\Media\Helpers\FileHelper::fieldUpload('space_page_search_banner',$settings['space_page_search_banner'] ?? "") !!}
                    </div>
                </div>
                <div class="form-group d-none">
                    <label class="" >{{__("Giao diện tìm kiếm")}}</label>
                    <div class="form-controls">
                        <select name="space_layout_search" class="form-control" >
                            <option value="normal" {{ ($settings['space_layout_search'] ?? '') == 'normal' ? 'selected' : ''  }}>{{__("Giao diện phổ thông")}}</option>
                            <option value="map" {{($settings['space_layout_search'] ?? '') == 'map' ? 'selected' : ''  }}>{{__('Giao diện bản đồ')}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="" >{{__("Tìm kiếm địa điểm")}}</label>
                    <div class="form-controls">
                        <select name="space_location_search_style" class="form-control">
                            <option {{ ($settings['space_location_search_style'] ?? '') == 'normal' ? 'selected' : ''  }}      value="normal">{{__("Phổ thông")}}</option>
                            <option {{ ($settings['space_location_search_style'] ?? '') == 'autocomplete' ? 'selected' : '' }} value="autocomplete">{{__('Tự động từ vị trí')}}</option>
                        </select>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @include('Space::admin.settings.map-search')
        <div class="panel">
            <div class="panel-title"><strong>{{__("Tùy chọn SEO")}}</strong></div>
            <div class="panel-body">
                <div class="form-group">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#seo_1">{{__("Tùy chọn chung")}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#seo_2">{{__("Chia sẻ trên Facebook")}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#seo_3">{{__("Chia sẻ trên Twitter")}}</a>
                        </li>
                    </ul>
                    <div class="tab-content" >
                        <div class="tab-pane active" id="seo_1">
                            <div class="form-group" >
                                <label class="control-label">{{__("Tiêu đề Seo")}}</label>
                                <input type="text" name="space_page_list_seo_title" class="form-control" placeholder="{{__("Nhập tiêu đề...")}}" value="{{ setting_item_with_lang('space_page_list_seo_title',request()->query('lang'))}}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{__("Mô tả Seo")}}</label>
                                <input type="text" name="space_page_list_seo_desc" class="form-control" placeholder="{{__("Nhập mô tả...")}}" value="{{setting_item_with_lang('space_page_list_seo_desc',request()->query('lang'))}}">
                            </div>
                            @if(is_default_lang())
                                <div class="form-group form-group-image">
                                    <label class="control-label">{{__("Ảnh nổi bật")}}</label>
                                    {!! \Modules\Media\Helpers\FileHelper::fieldUpload('space_page_list_seo_image', $settings['space_page_list_seo_image'] ?? "" ) !!}
                                </div>
                            @endif
                        </div>
                        @php
                            $seo_share = json_decode(setting_item_with_lang('space_page_list_seo_desc',request()->query('lang'),'[]'),true);
                        @endphp
                        <div class="tab-pane" id="seo_2">
                            <div class="form-group">
                                <label class="control-label">{{__("Tiêu đề trên Facebook")}}</label>
                                <input type="text" name="space_page_list_seo_share[facebook][title]" class="form-control" placeholder="{{__("Nhập tiêu đề...")}}" value="{{$seo_share['facebook']['title'] ?? "" }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{__("Mô tả trên Facebook")}}</label>
                                <input type="text" name="space_page_list_seo_share[facebook][desc]" class="form-control" placeholder="{{__("Nhập mô tả...")}}" value="{{$seo_share['facebook']['desc'] ?? "" }}">
                            </div>
                            @if(is_default_lang())
                                <div class="form-group form-group-image">
                                    <label class="control-label">{{__("Ảnh trên Facebook")}}</label>
                                    {!! \Modules\Media\Helpers\FileHelper::fieldUpload('space_page_list_seo_share[facebook][image]',$seo_share['facebook']['image'] ?? "" ) !!}
                                </div>
                            @endif
                        </div>
                        <div class="tab-pane" id="seo_3">
                            <div class="form-group">
                                <label class="control-label">{{__("Tiêu đề trên Twitter")}}</label>
                                <input type="text" name="space_page_list_seo_share[twitter][title]" class="form-control" placeholder="{{__("Nhập tiêu đề...")}}" value="{{$seo_share['twitter']['title'] ?? "" }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{__("Mô tả trên Twitter")}}</label>
                                <input type="text" name="space_page_list_seo_share[twitter][desc]" class="form-control" placeholder="{{__("Nhập mô tả...")}}" value="{{$seo_share['twitter']['title'] ?? "" }}">
                            </div>
                            @if(is_default_lang())
                                <div class="form-group form-group-image">
                                    <label class="control-label">{{__("Ảnh trên Twitter")}}</label>
                                    {!! \Modules\Media\Helpers\FileHelper::fieldUpload('space_page_list_seo_share[twitter][image]', $seo_share['twitter']['image'] ?? "" ) !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(is_default_lang())
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <h3 class="form-group-title">{{__("Tùy chọn đánh giá")}}</h3>
            <p class="form-group-desc">{{__('Đánh giá cấu hình cho coworking')}}</p>
        </div>
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="" >{{__("Cho phép đánh giá hệ thống coworking?")}}</label>
                        <div class="form-controls">
                            <label><input type="checkbox" name="space_enable_review" value="1" @if(!empty($settings['space_enable_review'])) checked @endif /> {{__("Đồng ý")}} </label>
                            <br>
                            <small class="form-text text-muted">{{__("Bật đánh giá coworking")}}</small>
                        </div>
                    </div>
                    <div class="form-group" data-condition="space_enable_review:is(1)">
                        <label class="" >{{__("Khách hàng phải đặt chỗ trước khi viết đánh giá?")}}</label>
                        <div class="form-controls">
                            <label><input type="checkbox" name="space_enable_review_after_booking" value="1"  @if(!empty($settings['space_enable_review_after_booking'])) checked @endif /> {{__("Đồng ý")}} </label>
                            <br>
                            <small class="form-text text-muted">{{__("Bật: Chỉ đăng đánh giá sau khi đặt phòng - Tắt: Đăng đánh giá mà không đặt trước")}}</small>
                        </div>
                    </div>
                    <div class="form-group" data-condition="space_enable_review:is(1)">
                        <label class="" >{{__("Đánh giá phải được sự chấp thuận của quản trị viên")}}</label>
                        <div class="form-controls">
                            <label><input type="checkbox" name="space_review_approved" value="1"  @if(!empty($settings['space_review_approved'])) checked @endif /> {{__("Đồng ý")}} </label>
                            <br>
                            <small class="form-text text-muted">{{__("Bật: Đánh giá phải được phê duyệt bởi quản trị viên - Tắt: Đánh giá được phê duyệt tự động")}}</small>
                        </div>
                    </div>
                    <div class="form-group" data-condition="space_enable_review:is(1)">
                        <label class="" >{{__("Số lượng đánh giá trên từng trang")}}</label>
                        <div class="form-controls">
                            <input type="number" class="form-control" name="space_review_number_per_page" value="{{ $settings['space_review_number_per_page'] ?? 5 }}" />
                            <small class="form-text text-muted">{{__("Chia các bình luận trong trang")}}</small>
                        </div>
                    </div>
                    <div class="form-group" data-condition="space_enable_review:is(1)">
                        <label class="" >{{__("Tiêu chí đánh giá")}}</label>
                        <div class="form-controls">
                            <div class="form-group-item">
                                <div class="g-items-header">
                                    <div class="row">
                                        <div class="col-md-5">{{__("Tiêu đề")}}</div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="g-items">
                                    <?php
                                    if(!empty($settings['space_review_stats'])){
                                    $social_share = json_decode($settings['space_review_stats']);
                                    ?>
                                    @foreach($social_share as $key=>$item)
                                        <div class="item" data-number="{{$key}}">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <input type="text" name="space_review_stats[{{$key}}][title]" class="form-control" value="{{$item->title}}" placeholder="{{__('Ví dụ: Dịch vụ')}}">
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <?php } ?>
                                </div>
                                <div class="text-right">
                                    <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> {{__('Thêm')}}</span>
                                </div>
                                <div class="g-more hide">
                                    <div class="item" data-number="__number__">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <input type="text" __name__="space_review_stats[__number__][title]" class="form-control" value="" placeholder="{{__('Ví dụ : Dịch vụ')}}">
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if(is_default_lang())
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <h3 class="form-group-title">{{__("Phí tùy chọn người đặt phòng")}}</h3>
            <p class="form-group-desc">{{__('Xác minh phí cho người đặt coworking')}}</p>
        </div>
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group-item">
                        <label class="control-label">{{__('Phí')}}</label>
                        <div class="g-items-header">
                            <div class="row">
                                <div class="col-md-5">{{__("Tên")}}</div>
                                <div class="col-md-3">{{__('Giá')}}</div>
                                <div class="col-md-3">{{__('Dạng')}}</div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>
                        <div class="g-items">
                            <?php  $languages = \Modules\Language\Models\Language::getActive();  ?>
                            @if(!empty($settings['space_booking_buyer_fees']))
                                <?php $space_booking_buyer_fees = json_decode($settings['space_booking_buyer_fees'],true); ?>
                                @foreach($space_booking_buyer_fees as $key=>$buyer_fee)
                                    <div class="item" data-number="{{$key}}">
                                        <div class="row">
                                            <div class="col-md-5">
                                                @if(!empty($languages) && setting_item('site_enable_multi_lang') && setting_item('site_locale'))
                                                    @foreach($languages as $language)
                                                        <?php $key_lang = setting_item('site_locale') != $language->locale ? "_".$language->locale : ""   ?>
                                                        <div class="g-lang">
                                                            <div class="title-lang">{{$language->name}}</div>
                                                            <input type="text" name="space_booking_buyer_fees[{{$key}}][name{{$key_lang}}]" class="form-control" value="{{$buyer_fee['name'.$key_lang] ?? ''}}" placeholder="{{__('Tên phí')}}">
                                                            <input type="text" name="space_booking_buyer_fees[{{$key}}][desc{{$key_lang}}]" class="form-control" value="{{$buyer_fee['desc'.$key_lang] ?? ''}}" placeholder="{{__('Mô tả phí')}}">
                                                        </div>

                                                    @endforeach
                                                @else
                                                    <input type="text" name="space_booking_buyer_fees[{{$key}}][name]" class="form-control" value="{{$buyer_fee['name'] ?? ''}}" placeholder="{{__('Tên phí')}}">
                                                    <input type="text" name="space_booking_buyer_fees[{{$key}}][desc]" class="form-control" value="{{$buyer_fee['desc'] ?? ''}}" placeholder="{{__('Mô tả phí')}}">
                                                @endif
                                            </div>
                                            <div class="col-md-3">
                                                <input type="number" min="0" name="space_booking_buyer_fees[{{$key}}][price]" class="form-control" value="{{$buyer_fee['price']}}">
                                            </div>
                                            <div class="col-md-3">
                                                <select name="space_booking_buyer_fees[{{$key}}][type]" class="form-control d-none">
                                                    <option @if($buyer_fee['type'] ==  'one_time') selected @endif value="one_time">{{__("Một lần")}}</option>
                                                </select>
                                                <label>
                                                    <input type="checkbox" min="0" name="space_booking_buyer_fees[{{$key}}][per_person]" value="on" @if($buyer_fee['per_person'] ?? '') checked @endif >
                                                    {{__("Giá theo từng ngừoi")}}
                                                </label>
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="text-right">
                            <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> {{__('Thêm')}}</span>
                        </div>
                        <div class="g-more hide">
                            <div class="item" data-number="__number__">
                                <div class="row">
                                    <div class="col-md-5">
                                        @if(!empty($languages) && setting_item('site_enable_multi_lang') && setting_item('site_locale'))
                                            @foreach($languages as $language)
                                                <?php $key = setting_item('site_locale') != $language->locale ? "_".$language->locale : ""   ?>
                                                <div class="g-lang">
                                                    <div class="title-lang">{{$language->name}}</div>
                                                    <input type="text" __name__="space_booking_buyer_fees[__number__][name{{$key}}]" class="form-control" value="" placeholder="{{__('Tên phí')}}">
                                                    <input type="text" __name__="space_booking_buyer_fees[__number__][desc{{$key}}]" class="form-control" value="" placeholder="{{__('Mô tả phí')}}">
                                                </div>

                                            @endforeach
                                        @else
                                            <input type="text" __name__="space_booking_buyer_fees[__number__][name]" class="form-control" value="" placeholder="{{__('Tên phí')}}">
                                            <input type="text" __name__="space_booking_buyer_fees[__number__][desc]" class="form-control" value="" placeholder="{{__('Mô tả phí')}}">
                                        @endif
                                    </div>
                                    <div class="col-md-3">
                                        <input type="number" min="0" __name__="space_booking_buyer_fees[__number__][price]" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3">
                                        <select __name__="space_booking_buyer_fees[__number__][type]" class="form-control d-none">
                                            <option value="one_time">{{__("Một lần")}}</option>
                                        </select>
                                        <label>
                                            <input type="checkbox" min="0" __name__="space_booking_buyer_fees[__number__][per_person]" value="on">
                                            {{__("Giá theo từng người")}}
                                        </label>
                                    </div>
                                    <div class="col-md-1">
                                        <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(is_default_lang())
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <h3 class="form-group-title">{{__("Tùy chọn")}}</h3>
            <p class="form-group-desc">{{__('Cấu hình chủ nhầ cho coworking')}}</p>
        </div>
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="" >{{__("coworking được tạo bởi nhà cung cấp phải được phê duyệt bởi quản trị viên")}}</label>
                        <div class="form-controls">
                            <label><input type="checkbox" name="space_vendor_create_service_must_approved_by_admin" value="1" @if(!empty($settings['space_vendor_create_service_must_approved_by_admin'])) checked @endif /> {{__("Đồng ý")}} </label>
                            <br>
                            <small class="form-text text-muted">{{__("Bật: Khi nhà cung cấp đăng một dịch vụ, nó cần phải được sự chấp thuận của quản trị viên")}}</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="" >{{__("Cho phép nhà cung cấp có thể thay đổi trạng thái đặt phòng của họ")}}</label>
                        <div class="form-controls">
                            <label><input type="checkbox" name="space_allow_vendor_can_change_their_booking_status" value="1" @if(!empty($settings['space_allow_vendor_can_change_their_booking_status'])) checked @endif /> {{__("Đồng ý")}} </label>
                            <br>
                            <small class="form-text text-muted">{{__("Bật: Nhà cung cấp có thể thay đổi trạng thái đặt phòng của họ")}}</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

         
@if(is_default_lang())
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__("Vô hiệu hóa module coworking?")}}</h3>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-title"><strong>{{__("Vô hiệu hóa module coworking")}}</strong></div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="form-controls">
                    <label><input type="checkbox" name="space_disable" value="1" @if(setting_item('space_disable')) checked @endif > {{__('Đồng ý')}}</label>
                    </div>
                </div>
            </div>
        </div>            
    </div>
</div>

@endif

