<h2 class="title-bar no-border-bottom">
    {{__("Báo cáo")}}
</h2>
@include('admin.message')
<div class="booking-history-manager">
    <div class="tabbable">
        <ul class="nav nav-tabs ht-nav-tabs">
            <?php $status_type = Request::query('status'); ?>
            <li class="@if(empty($status_type)) active @endif">
                <a href="{{ route("space.vendor.booking_report") }}">{{__("Tất cả booking")}}</a>
            </li>
            @if(!empty($statues))
                @foreach($statues as $status)
                    <li class="@if(!empty($status_type) && $status_type == $status) active @endif">
                        <a href="{{ route("space.vendor.booking_report",['status' => $status]) }}">{{booking_status_to_text($status)}}</a>
                    </li>
                @endforeach
            @endif
        </ul>
        @if(!empty($bookings) and $bookings->total() > 0)
            <div class="tab-content">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-booking-history">
                        <thead>
                        <tr>
                            <th width="2%">{{__("Loại")}}</th>
                            <th>{{__("Tiêu đề")}}</th>
                            <th class="a-hidden">{{__("Đặt ngày")}}</th>
                            <th class="a-hidden">{{__("Thời giant thực hiện")}}</th>
                            <th>{{__("Giá")}}</th>
                            <th class="a-hidden">{{__("Trạng thái")}}</th>
                            <th>{{__("Tuỳ chọn")}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bookings as $booking)
                            @include('Space::frontend.manageSpace.bookingReport.loop')
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="bravo-pagination">
                    {{$bookings->appends(request()->query())->links()}}
                </div>
            </div>
        @else
            {{__("Không có đơn đặt nào")}}
        @endif
    </div>
</div>
