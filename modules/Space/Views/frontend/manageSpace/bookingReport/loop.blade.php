<tr>
    <td class="booking-history-type">
        @if($service = $booking->service)
            <i class="{{$service->getServiceIconFeatured()}}"></i>
        @endif
        <small>{{$booking->object_model}}</small>
    </td>
    <td>
        @if($service = $booking->service)
            <a target="_blank" href="{{$service->getDetailUrl()}}">
                {{$service->title}}
            </a>
        @else
            {{__("[Đã xoá]")}}
        @endif
    </td>
    <td class="a-hidden">{{display_date($booking->created_at)}}</td>
    <td class="a-hidden">
        {{__("Ngày bắt đầu")}} : {{display_date($booking->start_date)}} <br>
        {{__("Ngày kết thúc")}} : {{display_date($booking->end_date)}} <br>
        {{__("Thời gian")}} :

        @if($booking->duration_nights <= 1)
            {{__(':count ngày',['count'=>$booking->duration_nights])}}
        @else
            {{__(':count ngày',['count'=>$booking->duration_nights])}}
        @endif
    </td>
    <td>{{format_money_main($booking->total)}}</td>
    <td class="{{$booking->status}} a-hidden">{{$booking->statusName}}</td>
    <td width="2%">
        @if($service = $booking->service)
            <a class="btn btn-xs btn-primary btn-info-booking" data-toggle="modal" data-target="#modal-booking-{{$booking->id}}">
                <i class="fa fa-info-circle"></i>{{__("Thông tin")}}
            </a>
            @include ($service->checkout_booking_detail_modal_file ?? '')
        @endif
        @if(!empty(setting_item("space_allow_vendor_can_change_their_booking_status")))
            <a class="btn btn-xs btn-info btn-make-as" data-toggle="dropdown">
                <i class="icofont-ui-settings"></i>
                {{__("Tuỳ chọn")}}
            </a>
            <div class="dropdown-menu">
                @if(!empty($statues))
                    @foreach($statues as $status)
                        <a href="{{ route("space.vendor.booking_report.bulk_edit" , ['id'=>$booking->id , 'status'=>$status]) }}">
                            <i class="icofont-long-arrow-right"></i> {{__(' :name',['name'=>booking_status_to_text($status)])}}
                        </a>
                    @endforeach
                @endif
            </div>
        @endif
    </td>
</tr>
