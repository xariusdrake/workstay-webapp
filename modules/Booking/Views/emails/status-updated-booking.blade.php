@extends('Email::layout')
@section('content')

    <div class="b-container">
        <div class="b-panel">
            @switch($to)
                @case ('admin')
                <h3 class="email-headline"><strong>{{__('Chào')}}</strong></h3>
                <p>{{__('The booking status has been updated')}}</p>
                @break

                @case ('vendor')
                <h3 class="email-headline"><strong>{{__('Chào :name',['name'=>$booking->vendor->nameOrEmail ?? ''])}}</strong></h3>
                <p>{{__('Trạng thái đơn đặt đã được cập nhật')}}</p>
                @break


                @case ('customer')
                <h3 class="email-headline"><strong>{{__('Chào :name',['name'=>$booking->first_name ?? ''])}}</strong></h3>
                <p>{{__('Trạng thái đơn đặt của bạn đã được cập nhật')}}</p>
                @break

            @endswitch

            @include($service->email_new_booking_file ?? '')
        </div>
    </div>
@endsection
