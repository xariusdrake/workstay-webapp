<?php
namespace Modules\Booking\Gateways;

use Illuminate\Http\Request;
use Modules\Booking\Events\BookingCreatedEvent;

class OfflinePaymentGateway extends BaseGateway
{
    public $name = 'Thanh toán trực tiếp';

    public function process(Request $request, $booking, $service)
    {
        $service->beforePaymentProcess($booking, $this);
        // Simple change status to processing
        $booking->markAsProcessing($this, $service);
        $booking->sendNewBookingEmails();

        event(new BookingCreatedEvent($booking));

        $service->afterPaymentProcess($booking, $this);
        return response()->json([
            'url' => $booking->getDetailUrl()
        ])->send();
    }

    public function getOptionsConfigs()
    {
        return [
            [
                'type'  => 'checkbox',
                'id'    => 'enable',
                'label' => __('Cho phép thanh toán trực tiếp?')
            ],
            [
                'type'  => 'input',
                'id'    => 'name',
                'label' => __('Tùy chỉnh tên'),
                'std'   => __("Thanh toán trực tiếp")
            ],
            [
                'type'  => 'upload',
                'id'    => 'logo_id',
                'label' => __('Tùy chỉnh Logo'),
            ],
            [
                'type'  => 'editor',
                'id'    => 'html',
                'label' => __('Tùy chỉnh HTML Description')
            ],
        ];
    }
}