<div class="form-group">
    <label>{{ __('Tiêu đề')}}</label>
    <input type="text" value="{{ $translation->title ?? 'New Post' }}" placeholder="Tiêu đề tin tức" name="title" class="form-control">
</div>
<div class="form-group">
    <label class="control-label">{{ __('Nội dung')}} </label>
    <div class="">
        <textarea name="content" class="d-none has-ckeditor" cols="30" rows="10">{{$translation->content}}</textarea>
    </div>
</div>
 