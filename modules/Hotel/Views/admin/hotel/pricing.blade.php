@if(is_default_lang())
    <div class="panel">
        <div class="panel-title"><strong>{{__("Giờ nhận/trả phòng")}}</strong></div>
        <div class="panel-body">
            <div class="form-group d-none">
                <label>{{__('Cho phép đặt phòng cả ngày')}}</label>
                <br>
                <label>
                    <input type="checkbox" name="allow_full_day" @if($row->allow_full_day) checked @endif value="1"> {{__("Cho phép đặt phòng cả ngày")}}
                </label>
                <div class="small">
                    {{__("Bạn có thể đặt phòng cả ngày")}} <br>
                    {{__("Ví dụ: đặt phòng từ 22-23, sau đó tất cả các ngày 22 và 23 đã đầy, người khác không thể đặt trước")}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{__("Thời gian nhận phòng")}}</label>
                        <input type="text" value="{{$row->check_in_time}}" placeholder="{{__("ví dụ: 12:00AM")}}" name="check_in_time" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{__("Thời gian trả phòng")}}</label>
                        <input type="text" value="{{$row->check_out_time}}" placeholder="{{__("Ví dụ: 11:00AM")}}" name="check_out_time" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-title"><strong>{{__("Giá cả")}}</strong></div>
        <div class="panel-body">
            @if(is_default_lang())
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="control-label">{{__("Giá")}}</label>
                            <input type="number" step="any" min="0" name="price" class="form-control" value="{{$row->price}}" placeholder="{{__("Giá tiền khách sạn")}}">
                        </div>
                    </div>
                    <div class="col-lg-6 d-none">
                        <div class="form-group">
                            <label class="control-label">{{__("Giá được giảm")}}</label>
                            <input type="number" step="any" name="sale_price" class="form-control" value="{{$row->sale_price}}" placeholder="{{__("Giá tiền khách sạn được giảm")}}">
                            <span><i>{{__("Nếu giá thông thường thấp hơn mức giảm giá, nó sẽ hiển thị giá thông thường")}}</i></span>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif
