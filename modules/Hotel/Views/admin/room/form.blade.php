@include('Hotel::admin.room.form-detail.content')
<hr>
@include('Hotel::admin.room.form-detail.pricing')
<hr>
@include('Hotel::admin.room.form-detail.attributes')
<hr>
@if(is_default_lang())
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label ><strong>{{__('Trạng thái')}}</strong> </label>
                <select name="status"  class="custom-select">
                    <option value="publish" >{{__('Công khai')}}</option>
                    <option value="pending"  @if($row->status == 'pending') selected @endif >{{__('Đang chờ xử lý')}}</option>
                    <option value="draft"  @if($row->status == 'draft') selected @endif >{{__('Bản nháp')}}</option>
                </select>
            </div>
        </div>
    </div>
@endif