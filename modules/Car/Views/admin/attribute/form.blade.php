<div class="form-group">
    <label>{{__("Tên")}}</label>
    <input type="text" value="{{$translation->name}}" placeholder="{{__("Tên thuộc tính")}}" name="name" class="form-control">
</div>
@if(is_default_lang())
    <div class="form-group">
        <label>{{__("Hiển thị dạng trong chi tiết dịch vụ")}}</label>
        <select name="display_type" class="form-control">
            <option  @if($row->display_type == "icon_left") selected @endif value="icon_left">{{__("Hiển thị biểu tượng bên trái")}}</option>
            <option  @if($row->display_type == "icon_center") selected @endif value="icon_center">{{__("Hiển thị biểu tượng ở giữa")}}</option>
        </select>
    </div>
    <div class="form-grou">
        <label>{{__('Ẩn trong chi tiết dịch vụ')}}</label>
        <br>
        <label>
            <input type="checkbox" name="hide_in_single" @if($row->hide_in_single) checked @endif value="1"> {{__("Ẩn")}}
        </label>
    </div>
@endif