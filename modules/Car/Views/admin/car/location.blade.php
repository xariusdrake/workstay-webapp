<div class="panel">
    <div class="panel-title"><strong>{{__("Địa điểm")}}</strong></div>
    <div class="panel-body">
        @if(is_default_lang())
            <div class="form-group">
                <label class="control-label">{{__("Địa điểm")}}</label>
                <div class="">
                    <select name="location_id" class="form-control">
                        <option value="">{{__("-- Xin hãy chọn --")}}</option>
                        <?php
                        $traverse = function ($locations, $prefix = '') use (&$traverse, $row) {
                            foreach ($locations as $location) {
                                $selected = '';
                                if ($row->location_id == $location->id)
                                    $selected = 'selected';
                                printf("<option value='%s' %s>%s</option>", $location->id, $selected, $prefix . ' ' . $location->name);
                                $traverse($location->children, $prefix . '-');
                            }
                        };
                        $traverse($car_location);
                        ?>
                    </select>
                </div>
            </div>
        @endif
        <div class="form-group">
            <label class="control-label">{{__("Địa chỉ")}}</label>
            <input type="text" name="address" class="form-control" placeholder="{{__("Địa chỉ")}}" value="{{$translation->address}}">
        </div>
        @if(is_default_lang())
            <div class="form-group">
                <label class="control-label">{{__("Công cụ bản đồ")}}</label>
                <div class="control-map-group">
                    <div id="map_content"></div>
                    <input type="text" placeholder="{{__("Tìm kiếm theo tên...")}}" class="bravo_searchbox form-control" autocomplete="off" onkeydown="return event.key !== 'Enter';">
                    <div class="g-control">
                        <div class="form-group">
                            <label>{{__("Vĩ độ bản đồ")}}:</label>
                            <input type="text" name="map_lat" class="form-control" value="{{$row->map_lat}}" onkeydown="return event.key !== 'Enter';">
                        </div>
                        <div class="form-group">
                            <label>{{__("kinh độ bản đồ")}}:</label>
                            <input type="text" name="map_lng" class="form-control" value="{{$row->map_lng}}" onkeydown="return event.key !== 'Enter';">
                        </div>
                        <div class="form-group">
                            <label>{{__("Thu phóng bản đồ")}}:</label>
                            <input type="text" name="map_zoom" class="form-control" value="{{$row->map_zoom ?? "8"}}" onkeydown="return event.key !== 'Enter';">
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
