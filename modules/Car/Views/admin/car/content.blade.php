<div class="panel">
    <div class="panel-title"><strong>{{__("Nội dung của xe")}}</strong></div>
    <div class="panel-body">
        <div class="form-group">
            <label>{{__("Tiêu đề")}}</label>
            <input type="text" value="{{$translation->title}}" placeholder="{{__("Tên của xe")}}" name="title" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">{{__("Nội dung")}}</label>
            <div class="">
                <textarea name="content" class="d-none has-ckeditor" cols="30" rows="10">{{$translation->content}}</textarea>
            </div>
        </div>
        @if(is_default_lang())
            <div class="form-group">
                <label class="control-label">{{__("Youtube Video")}}</label>
                <input type="text" name="video" class="form-control" value="{{$row->video}}" placeholder="{{__("Youtube link video")}}">
            </div>
        @endif
        <div class="form-group-item">
            <label class="control-label">{{__('Câu hỏi thường gặp')}}</label>
            <div class="g-items-header">
                <div class="row">
                    <div class="col-md-5">{{__("Tiêu đề")}}</div>
                    <div class="col-md-5">{{__('Nội dung')}}</div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="g-items">
                @if(!empty($translation->faqs))
                    @php if(!is_array($translation->faqs)) $translation->faqs = json_decode($translation->faqs); @endphp
                    @foreach($translation->faqs as $key=>$faq)
                        <div class="item" data-number="{{$key}}">
                            <div class="row">
                                <div class="col-md-5">
                                    <input type="text" name="faqs[{{$key}}][title]" class="form-control" value="{{$faq['title']}}" placeholder="{{__('Ví dụ: Khi nào và ở đâu chuyến tham quan kết thúc?')}}">
                                </div>
                                <div class="col-md-6">
                                    <textarea name="faqs[{{$key}}][content]" class="form-control" placeholder="...">{{$faq['content']}}</textarea>
                                </div>
                                <div class="col-md-1">
                                        <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="text-right">
                    <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> {{__('Thêm')}}</span>
            </div>
            <div class="g-more hide">
                <div class="item" data-number="__number__">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" __name__="faqs[__number__][title]" class="form-control" placeholder="{{__('Ví dụ: Tôi có thể mang theo thú cưng không?')}}">
                        </div>
                        <div class="col-md-6">
                            <textarea __name__="faqs[__number__][content]" class="form-control" placeholder=""></textarea>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(is_default_lang())
            <div class="form-group">
                <label class="control-label">{{__("Ảnh Banner")}}</label>
                <div class="form-group-image">
                    {!! \Modules\Media\Helpers\FileHelper::fieldUpload('banner_image_id',$row->banner_image_id) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{{__("Bộ Sưu Tập")}}</label>
                {!! \Modules\Media\Helpers\FileHelper::fieldGalleryUpload('gallery',$row->gallery) !!}
            </div>
        @endif
    </div>
</div>

@if(is_default_lang())
    <div class="panel">
        <div class="panel-title"><strong>{{__("Thông tin thêm")}}</strong></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>{{__("Hành khách")}}</label>
                        <input type="number" value="{{$row->passenger}}" placeholder="{{__("Ví dụ: 3")}}" name="passenger" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>{{__("Cần gạt số")}}</label>
                        <input type="text" value="{{$row->gear}}" placeholder="{{__("Ví dụ: Tự động")}}" name="gear" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>{{__("Hành lý")}}</label>
                        <input type="number" value="{{$row->baggage}}" placeholder="{{__("Ví dụ: 5")}}" name="baggage" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>{{__("Số cửa")}}</label>
                        <input type="number" value="{{$row->door}}" placeholder="{{__("Ví dụ: 4")}}" name="door" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif