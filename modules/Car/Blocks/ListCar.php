<?php
namespace Modules\Car\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Modules\Car\Models\Car;
use Modules\Location\Models\Location;

class ListCar extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Tiêu đề')
                ],
                [
                    'id'        => 'desc',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Mô tả')
                ],
                [
                    'id'        => 'number',
                    'type'      => 'input',
                    'inputType' => 'number',
                    'label'     => __('Số lượng xe')
                ],
                [
                    'id'            => 'style',
                    'type'          => 'radios',
                    'label'         => __('Phong cách'),
                    'values'        => [
                        [
                            'value'   => 'normal',
                            'name' => __("Normal")
                        ],
                        [
                            'value'   => 'carousel',
                            'name' => __("Slider Carousel")
                        ]
                    ]
                ],
                [
                    'id'      => 'location_id',
                    'type'    => 'select2',
                    'label'   => __('Lọc theo Địa điểm'),
                    'select2' => [
                        'ajax'  => [
                            'url'      => url('/admin/module/location/getForSelect2'),
                            'dataType' => 'json'
                        ],
                        'width' => '100%',
                        'allowClear' => 'true',
                        'placeholder' => __('-- Lựa chọn --')
                    ],
                    'pre_selected'=>url('/admin/module/location/getForSelect2?pre_selected=1')
                ],
                [
                    'id'            => 'order',
                    'type'          => 'radios',
                    'label'         => __('Order'),
                    'values'        => [
                        [
                            'value'   => 'id',
                            'name' => __("Ngày tạo")
                        ],
                        [
                            'value'   => 'title',
                            'name' => __("Tiêu đề")
                        ],
                    ]
                ],
                [
                    'id'            => 'order_by',
                    'type'          => 'radios',
                    'label'         => __('Xắp xếp'),
                    'values'        => [
                        [
                            'value'   => 'asc',
                            'name' => __("Giảm dần")
                        ],
                        [
                            'value'   => 'desc',
                            'name' => __("Tăng dần")
                        ],
                    ]
                ],
                [
                    'type'=> "checkbox",
                    'label'=>__("Chỉ duy nhất các xe đặc trưng?"),
                    'id'=> "is_featured",
                    'default'=>true
                ]
            ]
        ]);
    }

    public function getName()
    {
        return __('Xe: Danh sách xe');
    }

    public function content($model = [])
    {
        $model_car = Car::select("bravo_cars.*")->with(['location','translations','hasWishList']);
        if(empty($model['order'])) $model['order'] = "id";
        if(empty($model['order_by'])) $model['order_by'] = "desc";
        if(empty($model['number'])) $model['number'] = 5;
        if (!empty($model['location_id'])) {
            $location = Location::where('id', $model['location_id'])->where("status","publish")->first();
            if(!empty($location)){
                $model_car->join('bravo_locations', function ($join) use ($location) {
                    $join->on('bravo_locations.id', '=', 'bravo_cars.location_id')
                        ->where('bravo_locations._lft', '>=', $location->_lft)
                        ->where('bravo_locations._rgt', '<=', $location->_rgt);
                });
            }
        }

        if(!empty($model['is_featured']))
        {
            $model_car->where('is_featured',1);
        }

        $model_car->orderBy("bravo_cars.".$model['order'], $model['order_by']);
        $model_car->where("bravo_cars.status", "publish");
        $model_car->with('location');
        $model_car->groupBy("bravo_cars.id");
        $list = $model_car->limit($model['number'])->get();
        $data = [
            'rows'       => $list,
            'style_list' => $model['style'],
            'title'      => $model['title'],
            'desc'       => $model['desc'],
        ];
        return view('Car::frontend.blocks.list-car.index', $data);
    }
}
