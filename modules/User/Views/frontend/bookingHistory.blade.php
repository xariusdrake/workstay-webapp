@extends('layouts.user')
@section('head')

@endsection
@section('content')
    <h2 class="title-bar no-border-bottom">
        {{__("Lịch sử đặt phòng")}}
    </h2>
    @include('admin.message')
    <div class="booking-history-manager">
        <div class="tabbable">
            <ul class="nav nav-tabs ht-nav-tabs">
                <?php $status_type = Request::query('status'); ?>
                <li class="@if(empty($status_type)) active @endif">
                    <a href="{{url(app_get_locale()."/user/booking-history")}}">{{__("Tất cả đơn đặt phòng")}}</a>
                </li>
                @if(!empty($statues))
                    @foreach($statues as $status)
                        <li class="@if(!empty($status_type) && $status_type == $status) active @endif">
                            <a href="{{url(app_get_locale()."/user/booking-history?status=".$status)}}">{{booking_status_to_text($status)}}</a>
                        </li>
                    @endforeach
                @endif
            </ul>
            @if(!empty($bookings) and $bookings->total() > 0)
                <div class="tab-content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-booking-history">
                            <thead>
                            <tr>
                                <th width="2%">{{__("Dạng")}}</th>
                                <th>{{__("Tiêu đề")}}</th>
                                <th class="a-hidden">{{__("Ngày thực hiện")}}</th>
                                <th class="a-hidden">{{__("Thời gian thực hiện")}}</th>
                                <th>{{__("Giá")}}</th>
                                <th class="a-hidden">{{__("Trạng thái")}}</th>
                                <th>{{__("Tùy chọn")}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bookings as $booking)
                                @include(ucfirst($booking->object_model).'::frontend.bookingHistory.loop')
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="bravo-pagination">
                        {{$bookings->appends(request()->query())->links()}}
                    </div>
                </div>
            @else
                {{__("Không có lịch sử đặt phòng")}}
            @endif
        </div>
    </div>
@endsection
@section('footer')

@endsection
