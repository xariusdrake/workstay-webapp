@extends('layouts.user')
@section('head')
@endsection
@section('content')
    <h2 class="title-bar">
        {{__("Thay đổi mật khẩu")}}
    </h2>
    @include('admin.message')
    <form action="{{ route("user.change_password") }}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__("Mật khẩu cũ")}}</label>
                    <input type="password" name="current-password" placeholder="{{__("Mật khẩu cũ")}}" class="form-control">
                </div>
                <div class="form-group">
                    <label>{{__("Mật khẩu mới")}}</label>
                    <input type="password" name="new-password" placeholder="{{__("Mật khẩu mới")}}" class="form-control">
                                   </div>
                <div class="form-group">
                    <label>{{__("Nhập lại mật khẩu mới")}}</label>
                    <input type="password" name="new-password_confirmation" placeholder="{{__("Nhập lại mật khẩu mới")}}" class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <hr>
                <input type="submit" class="btn btn-primary" value="{{__("Thay đổi mật khẩu")}}">
                <a href="{{ route("user.profile.index") }}" class="btn btn-default">{{__("Đóng")}}</a>
            </div>
        </div>
    </form>
@endsection
@section('footer')

@endsection