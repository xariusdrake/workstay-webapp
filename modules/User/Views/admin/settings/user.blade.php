@if(is_default_lang())
    <div class="row">
        <div class="col-sm-4">
            <h3 class="form-group-title">{{__("Hệ thống nhắn tin")}}</h3>
            <p class="form-group-desc">{{__('Tùy chọn cấu hình nhắn tin')}}</p>
        </div>
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="" >{{__("Cho phép khách hàng có thể gửi tin nhắn cho chủ nahf trên trang chi tiết")}}</label>
                        <div class="form-controls">
                            <label><input type="checkbox" name="inbox_enable" value="1" @if(!empty($settings['inbox_enable'])) checked @endif /> {{__("Đồng ý")}} </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if(is_default_lang())
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <h3 class="form-group-title">{{__("Tùy chọn Google ReCapcha")}}</h3>
            <p class="form-group-desc">{{__('Cấu hình google recapcha cho hệ thống')}}</p>
        </div>
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="">{{__("reCapcha đăng nhập")}}</label>
                        <div class="form-controls">
                            <label><input type="checkbox" name="user_enable_login_recaptcha" value="1" @if(!empty($settings['user_enable_login_recaptcha'])) checked @endif /> {{__("Bật")}} </label>
                            <br>
                            <small class="form-text text-muted">{{__("Bật cho đăng nhập")}}</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="">{{__("reCapcha đăng ký")}}</label>
                        <div class="form-controls">
                            <label><input type="checkbox" name="user_enable_register_recaptcha" value="1" @if(!empty($settings['user_enable_register_recaptcha'])) checked @endif /> {{__("Bật")}} </label>
                            <br>
                            <small class="form-text text-muted">{{__("Bật cho đăng ký")}}</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
@endif
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__('Nội dung email đăng ký')}}</h3>
        <div class="form-group-desc">{{ __('Nội dung email gửi cho Khách hàng hoặc Quản trị viên khi người dùng đăng ký.')}}
            @foreach(\Modules\User\Listeners\SendMailUserRegisteredListen::CODE as $item=>$value)
                <div><code>{{$value}}</code></div>
            @endforeach
        </div>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">
                @if(is_default_lang())
                    <div class="form-group">
                        <label> <input type="checkbox" @if($settings['enable_mail_user_registered'] ?? '' == 1) checked @endif name="enable_mail_user_registered" value="1"> {{__("Cho phép gửi email cho khách hàng khi khách hàng đăng ký ?")}}</label>
                    </div>
                @else
                    <div class="form-group">
                        <label> <input type="checkbox" @if($settings['enable_mail_user_registered'] ?? '' == 1) checked @endif disabled name="enable_mail_user_registered" value="1"> {{__("Cho phép gửi email đến Khách hàng khi khách hàng đăng ký ?")}}</label>
                    </div>
                    @if($settings['enable_mail_user_registered'] != 1)
                        <p>{{__('Bạn phải cho phép trên ngôn ngữ chính.')}}</p>
                    @endif
                @endif

                <div class="form-group" data-condition="enable_mail_user_registered:is(1)">
                    <label>{{__("Email đến nội dung Khách hàng")}}</label>
                    <div class="form-controls">
                        <textarea name="user_content_email_registered" class="d-none has-ckeditor" cols="30" rows="10">{{setting_item_with_lang('user_content_email_registered',request()->query('lang')) ?? '' }}</textarea>
                    </div>
                </div>


                @if(is_default_lang())
                    <div class="form-group">
                        <label> <input type="checkbox" @if($settings['admin_enable_mail_user_registered'] ?? '' == 1) checked @endif name="admin_enable_mail_user_registered" value="1"> {{__("Cho phép gửi email đến Quản trị viên khi khách hàng đăng ký ?")}}</label>
                    </div>
                @else
                    <div class="form-group">
                        <label> <input type="checkbox" @if($settings['admin_enable_mail_user_registered'] ?? '' == 1) checked @endif disabled name="admin_enable_mail_user_registered" value="1"> {{__("Cho phép gửi email đến Quản trị viên khi khách hàng đăng ký ?")}}</label>
                    </div>
                        @if($settings['admin_enable_mail_user_registered'] != 1)
                            <p>{{__('Bạn phải cho phép trên ngôn ngữ chính.')}}</p>
                        @endif
                @endif
                <div class="form-group" data-condition="admin_enable_mail_user_registered:is(1)">
                    <label>{{__("Email đến nội dung Quản trị viên")}}</label>
                    <div class="form-controls">
                        <textarea name="admin_content_email_user_registered" class="d-none has-ckeditor" cols="30" rows="10">{{setting_item_with_lang('admin_content_email_user_registered',request()->query('lang'))?? '' }}</textarea>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-4">
        <h3 class="form-group-title">{{__('Nội dung Email khi người dùng quên mật khẩu')}}</h3>
        <div class="form-group-desc">
            @foreach(\Modules\User\Emails\ResetPasswordToken::CODE as $item=>$value)
                <div><code>{{$value}}</code></div>
            @endforeach
        </div>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-body">

                <div class="form-group">
                    <label>{{__("Nội dung")}}</label>
                    <div class="form-controls">
                        <textarea name="user_content_email_forget_password" class="d-none has-ckeditor" cols="30" rows="10">{{setting_item_with_lang('user_content_email_forget_password',request()->query('lang')) ?? '' }}</textarea>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
