@if(empty($row['id']))
<div class="form-group">
    <label>{{__("Trường ID")}} <span class="text-danger">*</span></label>
    <input type="text" value="{{$row['id'] ?? ''}}" placeholder="{{__("Trường ID ")}}" name="id" class="form-control" required>
    <i>{{__('Phải là duy nhất. Chỉ chấp nhận chữ và số, dấu gạch ngang, gạch dưới, không có dấu cách')}}</i>
    <div class="invalid-feedback">
        {{__('Vui lòng nhập trường id và đảm bảo nó là duy nhất')}}
    </div>
</div>
@else
    <input type="hidden" name="id" value="{{$row['id']}}">
@endif
<div class="form-group">
    <label>{{__("Tên trường")}} <span class="text-danger">*</span></label>
    <input type="text" value="{{$row['name'] ?? ''}}" placeholder="" name="name" class="form-control" required>
    <div class="invalid-feedback">
        {{__('Vui lòng nhập tên trường')}}
    </div>
</div>
<div class="form-group">
    <label>{{__("Dạng")}} <span class="text-danger">*</span></label>
    <select class="custom-select" name="type" required>
        <option value="text">{{__("Văn bản")}}</option>
        <option {{($row['type'] ?? '') == 'number' ? 'selected':''}} value="number">{{__("Số")}}</option>
        <option {{($row['type'] ?? '') == 'file' ? 'selected':''}} value="file">{{__("Tệp đính kèm")}}</option>
        <option {{($row['type'] ?? '') == 'multi_files' ? 'selected':''}} value="multi_files">{{__("Nhiều tệp đính kèm")}}</option>
    </select>
    <div class="invalid-feedback">
        {{__('Vui lòng chọn dạng trường')}}
    </div>
</div>
<div class="form-group">
    <label>{{__("Cho vai trò?")}} <span class="text-danger">*</span></label>
    <div class=" terms-scrollable">
        @foreach($roles as $role)
            <div>
                <label >
                     <input type="checkbox" name="roles[]" value="{{$role->id}}" @if(!empty($row['roles'] ?? []) and in_array($role->id,$row['roles'] ?? [])) checked @endif />{{ucfirst($role->name)}}
                </label>
            </div>
        @endforeach
    </div>
    <div class="invalid-feedback">
        {{__('Vui lòng chọn vai trò')}}
    </div>
</div>
<div class="form-group">
    <label>{{__("Bắt buộc?")}}</label>
    <select class="custom-select" name="required">
        <option value="">{{__("Không")}}</option>
        <option {{($row['required'] ?? '') == 1 ? 'selected':''}} value="1">{{__("Có")}}</option>
    </select>
</div>
<div class="form-group">
    <label>{{__("Số lượng")}}</label>
    <input type="text" value="{{$row['order'] ?? 0}}" placeholder="" name="order" class="form-control">
</div>
<div class="form-group">
    <label>{{__("Biểu tượng code")}}</label>
    <input type="text" value="{{$row['icon'] ?? ''}}" placeholder="{{__("Eg: fa fa-phone")}}" name="icon" class="form-control">
</div>
