@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between mb20">
            <h1 class="title-bar">{{ __('Sửa: :name',['name'=>$row['name'] ?? ''])}}</h1>
        </div>
        @include('admin.message')
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <form action="{{route('user.admin.role.verifyFieldsStore')}}" class="needs-validation" novalidate>
                    @csrf
                <div class="panel">
                    <div class="panel-title"><strong>{{ __('Sửa trường xác minh')}}</strong></div>
                    <div class="panel-body">
                        @include('User::admin.role.verifyFieldsForm')
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">{{__('Lưu')}}</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
