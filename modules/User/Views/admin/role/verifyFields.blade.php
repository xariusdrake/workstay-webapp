@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between mb20">
            <h1 class="title-bar">{{ __('Quản lý trường')}}</h1>
        </div>
        @include('admin.message')
        <div class="row">
            <div class="col-md-4">
                <form action="{{route('user.admin.role.verifyFieldsStore')}}" class="needs-validation" novalidate>
                    @csrf
                <div class="panel">
                    <div class="panel-title"><strong>{{__("Thêm")}}</strong></div>
                    <div class="panel-body">
                        @include('User::admin.role.verifyFieldsForm')
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">{{__('Thêm mới')}}</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-title">{{ __('Tất cả trường')}}</div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th width="60px"><input type="checkbox" class="check-all"></th>
                                <th>{{ __('ID')}}</th>
                                <th>{{ __('Biểu tượng')}}</th>
                                <th>{{ __('Tên')}}</th>
                                <th>{{ __('Dạng')}}</th>
                                <th>{{ __('Cho vai trò')}}</th>
                                <th>{{ __('Số lượng')}}</th>
                                <th>{{ __('Bắt buộc')}}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($fields as $id=>$row)
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ids[]" value="{{$id}}">
                                    </td>
                                    <td>{{$id}}</td>
                                    <td><i class="{{$row['icon'] ??''}}"></i></td>
                                    <td>{{$row['name']}}</td>
                                    <td>{{verify_type_to($row['type'],'name')}}</td>
                                    <td>@php
                                        if(!empty($row['roles'])){
                                            $roles = \Spatie\Permission\Models\Role::query()->whereIn('id',$row['roles'])->get();
                                            if(!empty($roles))
                                            {
                                                echo implode(", ",$roles->pluck('name')->toArray());
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <th>{{$row['order'] ?? 0}}</th>
                                    <td>{{$row['required'] ? __("Có") : 'Không'}}</td>
                                    <th><a href="{{route('user.admin.role.verifyFieldsEdit',['id'=>$id])}}" class="btn btn-primary btn-sm"> <i class="fa fa-edit"></i>  {{__('Sửa')}}</a></th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
