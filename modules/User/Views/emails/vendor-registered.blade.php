@extends('Email::layout')
@section('content')
<div class="b-container">
    <div class="b-panel">
        <h1>{{__("Xin chào Quản trị viên")}}</h1>

        <p>{{__('Một người dùng đã đăng ký là Nhà cung cấp. Vui lòng kiểm tra thông tin dưới đây:')}}</p>
        <ul>
            <li>{{__('Tên: :name',['name'=>$user->first_name])}}</li>
            <li>{{__('Họ: :name',['name'=>$user->last_name])}}</li>
            <li>{{__('Email: :email',['email'=>$user->email])}}</li>
            <li>{{__('Ngày đăng ký: :date',['date'=>display_date($user->created_at)])}}</li>
        </ul>
        <p>{{__('Bạn có thể phê duyệt yêu cầu ở đây:')}} <a href="{{url('admin/module/user/userUpgradeRequest')}}">{{__('Xem yêu cầu')}}</a></p>

        <br>
        <p>{{__('Trân trọng')}},<br>{{setting_item('site_title')}}</p>
    </div>
</div>
@endsection