@extends('Email::layout')
@section('content')
    <div class="b-container">
        <div class="b-panel">
            <h1>{{__("Xin chào :name",['name'=>$user->first_name])}}</h1>

            <p>{{__('Bạn nhận được email này vì chúng tôi đã chấp thuận yêu cầu đăng ký nhà cung cấp của bạn.')}}</p>
            <p>{{__('Bạn có thể kiểm tra tổng quan tại đây:')}} <a href="{{url('user/dashboard')}}">{{__('Xem tổng quan')}}</a></p>

            <br>
            <p>{{__('Trân trọng')}},<br>{{setting_item('site_title')}}</p>
        </div>
    </div>
@endsection
