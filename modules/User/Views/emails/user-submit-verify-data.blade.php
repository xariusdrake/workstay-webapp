@extends('Email::layout')
@section('content')
    <div class="b-container">
        <div class="b-panel">
            <h1>{{__("Xin chào Quản trị viên")}}</h1>

            <p>{{__('Một người dùng đã được gửi dữ liệu xác minh của họ.')}}</p>
            <p>{{__('Tên: :name',['name'=>$user->business_name ? $user->business_name : $user->first_name])}}</p>

            <p>{{__('Bạn có thể phê duyệt yêu cầu ở đây:')}} <a href="{{route('user.admin.verification.detail',['id'=>$user->id])}}">{{__('Xem yêu cầu')}}</a></p>

            <br>
            <p>{{__('Trân trọng')}},<br>{{setting_item('site_title')}}</p>
        </div>
    </div>
@endsection
