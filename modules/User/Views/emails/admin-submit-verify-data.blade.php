@extends('Email::layout')
@section('content')
    <div class="b-container">
        <div class="b-panel">
            <h1>{{__("Xin chào :name",['name'=>$user->business_name ? $user->business_name : $user->first_name])}}</h1>

            <p>{{__('Bạn nhận được email này vì chúng tôi đã cập nhật dữ liệu xác minh nhà cung cấp của bạn.')}}</p>
            <ul>
                @if(!empty($user->verification_fields))
                    @foreach($user->verification_fields as $field)
                        <li>
                            <strong>{{$field['name']}}:</strong>
                            <i>@if(!empty($field['is_verified'])) {{__("Đã xác minh")}} @else {{__("Chưa được xác minh")}} @endif</i>
                        </li>
                    @endforeach
                @endif
            </ul>
            <p>{{__('Bạn có thể kiểm tra thông tin tại đây:')}} <a href="{{route('user.verification.index')}}">{{__('xác minh dữ liệu')}}</a></p>

            <br>
            <p>{{__('Trân trọng')}},<br>{{setting_item('site_title')}}</p>
        </div>
    </div>
@endsection
