<?php
namespace Modules\Location\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Modules\Location\Models\Location;

class ListLocations extends BaseBlock
{
    function __construct()
    {
        $list_service = [];
        foreach (get_bookable_services() as $key => $service) {
            $list_service[] = ['value'   => $key,
                               'name' => ucwords($key)
            ];
        }
        $this->setOptions([
            'settings' => [
                [
                    'id'            => 'service_type',
                    'type'          => 'checklist',
                    'listBox'          => 'true',
                    'label'         => "<strong>".__('Loại dịch vụ')."</strong>",
                    'values'        => $list_service,
                ],
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Tiêu đề')
                ],
                [
                    'id'        => 'desc',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Mô tả')
                ],
                [
                    'id'        => 'number',
                    'type'      => 'input',
                    'inputType' => 'number',
                    'label'     => __('Số lượng')
                ],
                [
                    'id'            => 'layout',
                    'type'          => 'radios',
                    'label'         => __('Phong cách'),
                    'values'        => [
                        [
                            'value'   => 'style_1',
                            'name' => __("Style 1")
                        ],
                        [
                            'value'   => 'style_2',
                            'name' => __("Style 2")
                        ],
                        [
                            'value'   => 'style_3',
                            'name' => __("Style 3")
                        ],
                        [
                            'value'   => 'style_4',
                            'name' => __("Style 4")
                        ]
                    ]
                ],
                [
                    'id'            => 'order',
                    'type'          => 'radios',
                    'label'         => __('Order'),
                    'values'        => [
                        [
                            'value'   => 'id',
                            'name' => __("Ngày tạo")
                        ],
                        [
                            'value'   => 'name',
                            'name' => __("Tiêu đề")
                        ],
                    ],
                ],
                [
                    'id'            => 'order_by',
                    'type'          => 'radios',
                    'label'         => __('Xắp xếp'),
                    'values'        => [
                        [
                            'value'   => 'asc',
                            'name' => __("Giảm dần")
                        ],
                        [
                            'value'   => 'desc',
                            'name' => __("Tăng dần")
                        ],
                    ],
                ],
                [
                    'id'           => 'custom_ids',
                    'type'         => 'select2',
                    'label'        => __('Danh sách địa điểm theo IDs'),
                    'select2'      => [
                        'ajax'     => [
                            'url'      => route('location.admin.getForSelect2'),
                            'dataType' => 'json'
                        ],
                        'width'    => '100%',
                        'multiple' => "true",
                    ],
                    'pre_selected' => route('location.admin.getForSelect2', [
                        'pre_selected' => 1
                    ])
                ],
                [
                    'type'=> "checkbox",
                    'label'=>__("Liên kết đến trang chi tiết vị trí?"),
                    'id'=> "to_location_detail",
                    'default'=>false
                ]
            ]
        ]);
    }

    public function getName()
    {
        return __('Danh sách địa điểm');
    }

    public function content($model = [])
    {
        if(empty($model['order'])) $model['order'] = "id";
        if(empty($model['order_by'])) $model['order_by'] = "desc";
        if(empty($model['number'])) $model['number'] = 5;
        if (empty($model['service_type']))
            return '';
        $model_location = Location::query()->with(['translations']);
        $model_location->where("status","publish");
        if(!empty( $model['custom_ids'] )){
            $model_location->whereIn("id",$model['custom_ids']);
        }
        $model_location->orderBy($model['order'], $model['order_by']);
        $list = $model_location->limit($model['number'])->get();
        $data = [
            'rows'         => $list,
            'title'        => $model['title'],
            'desc'         => $model['desc'] ?? "",
            'service_type' => $model['service_type'],
            'layout'       => !empty($model['layout']) ? $model['layout'] : "style_1",
            'to_location_detail'=>$model['to_location_detail'] ?? ''
        ];
        return view('Location::frontend.blocks.list-locations.index', $data);
    }
}